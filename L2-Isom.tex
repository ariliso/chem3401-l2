\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1in]{geometry}
\usepackage{indentfirst}
\usepackage{chemfig}
\usepackage{chemmacros}
\usepackage{caption}
\usepackage{achemso}
\usepackage{microtype}
\usepackage{amsmath}
\usepackage[section]{placeins}
\usepackage{varioref}
\usepackage[capitalise,noabbrev]{cleveref}
\usepackage{booktabs}

\usechemmodule{units,redox,reactions}

\renewcommand*{\thefootnote}{\alph{footnote}} %letters instead of numbers for footnotes
\setkeys{acs}{articletitle = true}

\sisetup{
  round-half      = even,
  table-auto-round,
  multi-part-units= single,
  list-units      = single,
  range-units     = single
}

\captionsetup[figure]{width=0.8\linewidth}
\captionsetup[table]{width=0.8\linewidth}

\author{Ariel Lisogorsky - 3061877}

\title{Synthesis and Investigation of IR spectra of cis- and trans- geometric isomers of 
	\iupac{dichlorido|bis(ethylenediamine)|cobalt(III) chloride}}

\date{}

\begin{document}
\maketitle{}
\begin{abstract}
The use of IR to distinguish cis- and trans- geometric isomers of \iupac{dichlorido|\
bis(ethylenediamine)|cobalt(III) chloride} was investigated. \iupac{trans-dichlorido|\
bis(ethylenediamine)|cobalt(III) chloride} was synthesised by oxidation and chelation  of
\ch{CoCl2.6 H2O} in an acidic aqueous solution containing ethylenediamine. A low yield of 
\SI{4.43}{\percent} was observed. Carbonate solution was used to promote conversion of  the trans-
isomer synthesised to the cis- isomer. It was hypothesised that the higher symmetry trans- isomer
would have simpler absorption bands than the cis- isomer. The regions corresponding to \ch{C-NH2}
\ch{Co-Cl} bands in the spectrum are a close match to literature values. The spectra collected also
supported the hypothesis proposed.
\end{abstract}

\section{Introduction}

Geometric isomerism results from different possible arrangements of ligands around an atom. Unlike
stereoisomers which are non-superimposable mirror images of the same atomic arrangement, geometric
ligand environments vary among isomers. Geometric isomerism results in compounds with identical
composition and bonding but may have different physical, spectral and reactive properties. Symmetry,
which plays a large role in the spectral characteristics of inorganic complexes, can vary across
geometric isomers and may be used to distinguish products by their spectra.\cite{Shriver2014Inorganic}

A family of compounds that exhibit easily identifiable geometric isomerism are the 
\iupac{dichlorido|bis(ethylenediamine)|cobalt(III) chloride} (\ch{[Co(en)2Cl2]Cl})
family of compounds. \ch{[Co(en)2Cl2]Cl} can exist both in a cis- and a trans- geometric isomers.
Both the cis- and the trans- isomers exhibit some degree of symmetry but the trans- isomer a much 
greater degree of symmetry.The cis- isomer has a point group of $C_{2}$ while the point group of the 
trans- isomer varies between $C_{2h}$ and $D_2$ depending on the anion present in the complex
\cite{Sharma2006Second,Sharma2006Seconda}. The higher degree of symmetry present in the trans- 
isomer should correspond to less IR absorption bands than the cis- isomer\cite{Hughes1966InfraRed}.

Two absorption bands that would make good candidates for identification are the stretching modes of
\ch{C-NH2} or \ch{Co-Cl} bonds expected to appear in the regions from \SIrange{3300}{3500}{\per\cm}
as sharp peaks\cite{Mcmurry2010Organic} or \SIrange{200}{600}{\per\cm} for the two types of bonds 
respectively\cite{Hughes1966InfraRed,Shriver2014Inorganic}. The literature suggests that the 
\SIrange{200}{600}{\per\cm} region corresponding to \ch{Co-Cl} stretching is the most effective
identification method for geometric isomers of Cobalt(III)

\section{Materials and Method}

	\begin{table}[h]
		\centering
		\caption{Materials used in experiment and sources. Note: U of W denotes stocks 
			prepared by the university of Winnipeg}
		\label{tbl:mat}
			\begin{tabular}{lccl} \toprule
			Name & CAS No. & Supplier & Appearance \\ \midrule
			\ch{CoCl2.6 H2O} &  7791-13-1 & TJ Baker & Intense violet granular solid \\
			\SI{10}{\percent} Ethylenediamine  	 &  & U of W & Colourless transparent liquid \\
			\ch{HCl} (\SI{12}{\Molar}) & & U of W & Colourless transparent liquid \\
			Diethyl ether & & U of W & Colourless transparent liquid  \\
			Saturated \ch{Na2CO3 \aq{}}  & & U of W & transparent liquid with white crystals  \\ 
			\bottomrule	
			\end{tabular}
	\end{table}

\SI{0.4512}{\g} of Cobalt(II)Chloride Hexahydrate was added to \SI{2.5}{\mL} of water where it 
partially dissolved to form a pink solution. On addition of \SI{1.5}{\mL} of \SI{10}{\percent}
ethylenediamine to the solution, the remaining solid dissolved to form a dark burgundy solution.
Suction was used to draw air through the solution and the solution was placed in a \SI{90}{\celsius}
water bath. The solution was heated for an hour while deionized (DI) water was added to maintain
solution volume at initial levels. After an hour elapsed, the solution was allowed to cool slightly,
\SI{1}{\mL} of concentrated \ch{HCl} was added and the solution was returned to the water bath. As
solution volume decreased, colour in the solution progressed from burgundy through violet, blue, 
green before finally becoming cyan as crystals started to develop. The solution was cooled in an ice
bath and allowed to crystallize. Forest Green crystals were collected with vacuum filtration,
filtrate solution was pale green. Ice cold diethyl ether was used to wash the product. Significant
loss of green solid to filtrate was observed during diethyl ether wash. The remaining solid product
was collected and left to dry in the dark for seven days. The dried product was weighed and used to
press a \ch{KBr} pellet.

The remaining product was supplemented with additional \iupac{trans-dichlorido|bis(ethylenediamine)\
|cobalt(III) chloride} synthesised with the same method. The solid green product was added to
approximately \SI{2}{\mL} of DI water and allowed to dissolve over 10 minutes. Two drops of
saturated \ch{Na2CO3} solution were added to the solution. Colourless gas was evolved on contact and
the solution changed to a strong magenta colour. The solution was then dried on a watch glass placed
above boiling water. Magenta crystals were collected and used to produce a \ch{KBr} pellet.

IR spectra ( $\lambda =$~\SIrange{400}{4000}{\per\cm}) of both pellets were collected with a Brucker
FT-IR spectrophotometer. Peaks in potential regions of interest were identified and characterised.



\section{Observations and Results}

Only \SI{0.0240}{\g} of product was recovered in the initial synthesis (\SI{4.43}{\percent} yield).
An unknown amount of additional trans- product was added to the initial step and insufficient cis-
was not sufficiently dry to weigh before use therefore the yield of the trans- cis- conversion is
unknown

Both IR spectra collected for the samples showed a characteristic broad OH peak centred at 
\SI{3500}{\per\cm}. Near the OH peak, in the \SIrange{3300}{3500}{\per\cm} \ch{C-N} stretching
area of the spectrum, the spectrum of the trans- isomer had two sharp peaks where the cis- isomer
produced a broader, more complicated signal. Similarly, in the other area of interest (\ch{Co-Cl}
stretching from \SIrange{200}{600}{\per\cm}) the spectrum of the trans- isomer had two sharp intense
peaks while the cis- spectrum had a broader more complex region with two discernible maxima. The 
most prominent absorption peaks on the spectrum of the cis- isomer occurred between 
\SIrange[range-phrase = ~and~]{1300}{1500}{\per\cm}.
 
\begin{figure}[p]
	\centering
	\includegraphics[width=0.8\linewidth]{Arieltrans.pdf}
	\caption{IR absorption spectrum of \iupac{trans-dichlorido|bis(ethylenediamine)|cobalt(III) |
		chloride} in \ch{KBr} pellet. Dotted vertical lines enclose regions of spectrum corresponding
		to energies of \ch{C-NH2} and \ch{Co-Cl} stretching modes.}
	\label{fig:trans}
\end{figure}

\begin{figure}[p]
	\centering
	\includegraphics[width=0.8\linewidth]{Arielcis.pdf}
	\caption{IR absorption spectrum of \iupac{cis-dichlorido|bis(ethylenediamine)|cobalt(III) |
		chloride} in \ch{KBr} pellet. Dotted vertical lines enclose regions of spectrum corresponding
		to energies of \ch{C-NH2} and \ch{Co-Cl} stretching modes. Enlarged excerpts of regions of
		interest shown above spectrum in grater detail.}
	\label{fig:cis}
\end{figure}

\section{Discussion & Conclusion}
	Product loss during the initial synthesis of the trans- product was extremely high. The largest
	source of product loss observed in the method was during the filtration, recrystallization and
	washing step. The first of the major observed sources of product loss in the method was the 
	recrystallization step. After recrystallization, there appeared to be evidence that significant 
	product remained in the filtrate. A longer recrystallization time would have likely helped more 
	product come out of solution. The largest single source of loss observed when the solid was
	filtered and washed with cold diethyl ether. It appeared that approximately half the product
	produced was rapidly washed in to filtrate after addition of only three drops of diethyl ether.
	Three possible reasons that such a loss may  have occurred are vaccum intensity, solvent
	temperature and contamination of solvent or equipment.

	The features of the IR spectra of the synthesized species matched those of spectra in literature.
	Particularly, the method proposed by \citeauthor{Hughes1966InfraRed} matched the expected
	products of the syntheses. This suggests that both the syntheses and the identification method 
	were likely successful. The higher symmetry trans- isomer produced fewer, simpler IR bands than
	the cis- isomer in the regions expected for their bonds. The large central absorbance peaks in
	the cis- isomer may have resulted from the carbonate salt used to neutralize the acid used to 
	favor formation of the cis- isomer.

	In conclusion, IR spectra of the two geometric isomers behaved as expected and were similar to
	behaviors discussed in the literature.

\section{Appendix}

	\subsection{Reactions}

	\begin{reactions}
		4 Co^{2+} \aq{} + O2 \gas{} + 4 H+ \aq{} &<<=> 4 Co^{3+} \aq{} + 2 H2O \liquid{} \\
		Co^{3+} \aq{} + 3 Cl- \aq{} + 2 H2N-C2H4-NH2 \aq{} &-> [Co(H2N-C2H4-NH2)2Cl2]Cl \aq{}
	\end{reactions}

	\subsection{Calculations}
	\begin{align}
		\SI{0.4512}{\g}\ \ch{CoCl2.6H2O} \times \frac{\SI{1}{\mol}}{\SI{237.93}{\g}} &=
		\SI{1.896e-3}{\mol}\ \ch{Co3+} \\
		\SI{1.896e-3}{\mol}\ \ch{Co3+} \times {\SI{285.49}{\g\per\mol}} &=
		\SI{0.5414}{\g}\ \ch{[Co(en)2Cl2]Cl} \\
		\frac{\SI{0.0240}{\g}}{\SI{0.5414}{\g}} = 0.0443 &= \SI{4.43}{\percent}\ \textrm{of
		theoretical trans- product yield}
	\end{align}
\bibliography{Remote}
\end{document}